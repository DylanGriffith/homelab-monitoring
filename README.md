# Homelab monitoring

Stuff for monitoring my homelab environment.

# monitor-backups.rb

Uses the mtime of all files in a directory to determine the last time a
directory was backed up to. If the configured deadline is exceeded it writes to
stderr. It's designed to be running in multiple places and they monitor each
other as well by touching a file every time they run. This is meant to be run
from containers/VMs with shared storage (e.g. NFS).


1. Create a directory to store monitoring config and metadata (e.g. `/monitoring`)
1. Create a `backup-config.yml` in the above monitoring directory (see
   [`backup-config.yml.example`](backup-config.yml.example))
1. Run `MONITOR_BACKUPS_ID='monitor-backups-01' MONITORING_DIR='/monitoring' ./monitor-backups.rb`
1. Deploy multiple instances of the monitor process on multiple servers (one per
   `monitors` in the config file). They also monitor each other
1. Add the monitor command to crontab with `> /dev/null` on the end. All errors
   go to STDERR so you should ensure crontab is configured to email you when
   there is output.
