#!/usr/bin/env ruby

# frozen_string_literal: true

require 'logger'
require 'yaml'
require 'pathname'
require 'fileutils'

class MonitorBackups
  MONITOR_DAYS_LIMIT = 2 # Each monitor should run daily so warn if it's been longer than 2 days

  DAY = 24*60*60

  def initialize(
    logger: Logger.new(STDOUT),
    monitor_id: ENV.fetch("MONITOR_BACKUPS_ID"),
    monitoring_dir: ENV.fetch("MONITORING_DIR"),
    monitor_config_yml: File.read(Pathname(monitoring_dir).join("backup-config.yml"))
  )
    @logger = logger
    @monitor_id = monitor_id
    @monitoring_dir = Pathname(monitoring_dir)
    @monitoring_monitors_dir = @monitoring_dir.join("monitors")
    @monitor_config_yml = monitor_config_yml
    @errors = []
  end

  def run
    @logger.info("Starting backup monitor")

    make_monitors_dir_if_missing

    check_monitors

    check_backups

    update_monitor_id

    @logger.info("Finished backup monitor")
    @errors
  end

  private

  def make_monitors_dir_if_missing
    @monitoring_monitors_dir.mkdir unless @monitoring_monitors_dir.directory?
  end

  def too_old_dir?(dir, days:)
    largest_mtime(dir) < days_ago(days)
  end

  def too_old_file?(file, days:)
    File.mtime(file.to_s) < days_ago(days)
  end

  def largest_mtime(dir)
    max_mtime = Time.at(0)

    Pathname(dir).find do |path|
      mtime = File.mtime(path)
      max_mtime = mtime if mtime > max_mtime
    end

    max_mtime
  end

  def days_ago(days)
    Time.now - days * DAY
  end

  def check_monitors
    config[:monitors].each do |monitor|
      @logger.debug("Checking monitor: #{monitor[:id]}")
      if too_old_file?(monitor_id_file(monitor[:id]), days: MONITOR_DAYS_LIMIT)
        error = "monitor_failure: #{monitor[:id]} has not run in the last #{MONITOR_DAYS_LIMIT} days - #{monitor[:error_message]}"
        @logger.error(error)
        @errors << error
      end
    end
  end

  def check_backups
    config[:backups].each do |backup|
      @logger.debug("Checking backup: #{backup[:path]}")
      if too_old_dir?(backup[:path], days: backup[:error_after_days])
        error = "backup_failure: #{backup[:path]} has not backed up in the last #{backup[:error_after_days]} days - #{backup[:error_message]}"
        @logger.error(error)
        @errors << error
      end
    end
  end

  def update_monitor_id
    FileUtils.touch(monitor_id_file)
  end

  def monitor_id_file(monitor_id = @monitor_id)
    file = @monitoring_monitors_dir.join(monitor_id)
    if !file.exist? # Make it the first time we run the script to avoid errors
      FileUtils.touch(file.to_s)
    end
    file
  end

  def config
    @config ||= YAML.load(@monitor_config_yml, symbolize_names: true)
  end
end

if __FILE__ == $0
  errors = MonitorBackups.new.run
  errors.each do |error|
    $stderr.puts(error)
  end
end
