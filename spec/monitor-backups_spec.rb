require 'spec_helper'
require './monitor-backups'

RSpec.describe MonitorBackups do
  let(:logger) { instance_double(Logger, debug: nil, error: nil, info: nil, warn: nil) }
  let(:monitor_backups) { described_class.new(logger: logger, monitor_id: monitor_id, monitoring_dir: monitoring_dir, monitor_config_yml: monitor_config_yml) }
  let(:monitoring_dir) { Pathname("spec/tmp/monitoring") }
  let(:monitoring_monitors_dir) { monitoring_dir.join("monitors") }
  let(:monitor_id) { 'monitor-backups-03' }

  let(:monitor_config_yml) {
    <<~YML
    monitors:
      - id: monitor-backups-01
        error_message: 'check monitor-backups-01 is running'
      - id: monitor-backups-02
        error_message: 'the 2nd backup server could be down'
      - id: monitor-backups-03
        error_message: 'the 3rd backup server could be down'
    
    backups:
      - path: 'spec/tmp/photos'
        error_message: 'your photos backup broke'
        error_after_days: 3
      - path: 'spec/tmp/documents'
        error_message: 'your documents backup failed'
        error_after_days: 3
      - path: 'spec/tmp/homeassistant'
        error_message: 'your home assistant has not been backed up recently'
        error_after_days: 10
      - path: 'spec/tmp/family'
        error_message: 'something is wrong with your family backup'
        error_after_days: 10
    YML
  }

  before do
    # All files new enough
    with_old_dir("spec/tmp/photos") do |d|
      make_file(dir: d, days_ago: 1)
      make_file(dir: d, days_ago: 4)
    end

    # All files too old
    with_old_dir("spec/tmp/documents") do |d|
      make_file(dir: d, days_ago: 6)
      make_file(dir: d, days_ago: 4)
    end

    # One file new enough in a subdirectory
    with_old_dir("spec/tmp/homeassistant") do |d|
      make_file(dir: d, days_ago: 11)
      make_file(dir: d, days_ago: 13)
      with_old_dir("spec/tmp/homeassistant/subfolder") do |s|
        make_file(dir: s, days_ago: 8)
      end
    end

    # All files too old
    with_old_dir("spec/tmp/family") do |d|
      make_file(dir: d, days_ago: 11)
      make_file(dir: d, days_ago: 14)
      with_old_dir("spec/tmp/family/subfolder") do |s|
        make_file(dir: s, days_ago: 13)
      end
    end

    # monitor-backups-02 did not backup in the last 2 days
    monitoring_dir.mkdir
    monitoring_monitors_dir.mkdir
    make_file(file: monitoring_monitors_dir.join("monitor-backups-01"), hours_ago: 2)
    make_file(file: monitoring_monitors_dir.join("monitor-backups-02"), days_ago: 3)
    make_file(file: monitoring_monitors_dir.join("monitor-backups-03"), days_ago: 1)

    allow(logger).to receive(:debug)
    allow(logger).to receive(:info)
    allow(logger).to receive(:error)
    allow(logger).to receive(:warn)
  end

  describe '#run' do
    let(:expected_errors) do
      [
        'monitor_failure: monitor-backups-02 has not run in the last 2 days - the 2nd backup server could be down',
        'backup_failure: spec/tmp/documents has not backed up in the last 3 days - your documents backup failed',
        'backup_failure: spec/tmp/family has not backed up in the last 10 days - something is wrong with your family backup',
      ]
    end

    it 'logs' do
      expect(logger).to receive(:info) .with("Starting backup monitor")
      expect(logger).to receive(:debug).with("Checking monitor: monitor-backups-01")
      expect(logger).to receive(:debug).with("Checking monitor: monitor-backups-02")
      expect(logger).to receive(:debug).with("Checking monitor: monitor-backups-03")
      expect(logger).to receive(:debug).with("Checking backup: spec/tmp/photos")
      expect(logger).to receive(:debug).with("Checking backup: spec/tmp/documents")
      expect(logger).to receive(:debug).with("Checking backup: spec/tmp/homeassistant")
      expect(logger).to receive(:debug).with("Checking backup: spec/tmp/family")
      expect(logger).to receive(:error).with(expected_errors[0])
      expect(logger).to receive(:error).with(expected_errors[1])
      expect(logger).to receive(:error).with(expected_errors[2])
      expect(logger).to receive(:info) .with("Finished backup monitor")

      monitor_backups.run
    end

    it 'touches $monitoring_dir/$id' do
      monitor_backups.run

      expect(File.mtime(monitoring_monitors_dir.join(monitor_id))).to be > (Time.now - 60)
    end

    it 'returns errors' do
      result = monitor_backups.run

      expect(result).to eq(expected_errors)
    end

    it 'creates monitoring dir and monitor files when absent' do
      monitoring_monitors_dir.rmtree

      monitor_backups.run

      expect(monitoring_monitors_dir).to be_directory
      expect(monitoring_monitors_dir.join("monitor-backups-01")).to be_file
      expect(monitoring_monitors_dir.join("monitor-backups-02")).to be_file
      expect(monitoring_monitors_dir.join("monitor-backups-03")).to be_file
    end
  end
end
