# frozen_string_literal: true
require 'pathname'
require 'fileutils'
require 'securerandom'

module SpecDirectoryHelpers
  DAY = 24*60*60
  HOUR = 60*60

  # Make a randomly named file in this path
  def make_file(dir: nil, file: nil, days_ago: nil, hours_ago: nil)
    mtime =if days_ago
      Time.now - days_ago * DAY
    elsif hours_ago
      Time.now - hours_ago * HOUR
    else
      raise "No mtime given"
    end
    filename = if file
                 file
               elsif dir
                 dir.join("test-file-#{SecureRandom.hex.slice(0..10)}")
               else
                 raise "No dir or file given"
               end
    FileUtils.touch filename.to_s, mtime: mtime
  end

  # Need to yield to allow creating files before setting mtime. Otherwise the
  # mtime is updated when you next create a file in the directory.
  def with_old_dir(dir)
    FileUtils.mkdir_p(dir.to_s)

    yield Pathname.new(dir)

    FileUtils.touch dir.to_s, mtime: Time.now - 100 * DAY
  end
end

RSpec.configure do |c|
  c.before do
    Pathname.new("spec/tmp").rmtree
    Pathname.new("spec/tmp").mkdir
  end

  c.after do
    Pathname.new("spec/tmp").rmtree
  end

  c.include(SpecDirectoryHelpers)
end
